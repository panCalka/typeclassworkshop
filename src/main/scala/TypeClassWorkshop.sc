//- why we need it

//“A type class is an interface that defines some behavior.
//  More specifically, a type class specifies a bunch of functions, and when we decide to make a
//type an instance of a type class, we define what those functions mean for that type.”
//
//Learn You a Haskell for Great Good!

//The book Advanced Scala with Cats defines a type class as a programming technique that lets you add new behavior to closed data types without using inheritance,
//and without having access to the original source code of those types. Strictly speaking,
//this isn’t a technique that’s limited to functional programming, but it’s used so much in the Cats library

//- how to do it - easy way
// To impement type class we need 3 basic things
//1) A signature
//2) Implemantations for supported types
//3) Function that requires a type class

// Lets make an example with equal method

//Excercise - You have 10min
//Scala provides a toString method to let us convert any value to a String.
//However, this method comes with a few disadvantages: it is implemented for
//  every type in the language, many implementations are of limited use, and we
//  can’t opt-in to specific implementations for specific types.
//Let’s define a Printable type class to work around these problems:
//  1. Define a type class Printable[A] containing a single method format.
//format should accept a value of type A and return a String.
//2. Create an object PrintableInstances containing instances of
//Printable for String and Int.
//3. Define an object Printable with two generic interface methods:
//  format accepts a value of type A and a Printable of the corresponding
//type. It uses the relevant Printable to convert the A to a String.
//print accepts the same parameters as format and returns Unit. It
//prints the A value to the console using println.


//Using the Cats Library
// Let’s define an “application” now that uses the library.
//  First we’ll define a data type to represent a well-known type of furry animal:
final case class Cat(name: String, age: Int, color: String)
//Next we’ll create an implementation of Printable for Cat that returns content
//  in the following format:
//  NAME is a AGE year-old COLOR cat.
//  Finally, use the type class on the console or in a short demo app: create a Cat
//and print it to the console:
// Define a cat:
//val cat = Cat(/* ... */)
// Print the cat!
//FIrst in standard typeClass from Scala - 5min


//Type-Classes in Cats
import cats.{Show}
import cats.instances.int._
import cats.instances.string._
import cats.syntax.show._

implicit val catShow = Show.show[Cat] { cat =>
  val name = cat.name.show
  val age = cat.age.show
  val color = cat.color.show
  s"$name is a $age year-old $color cat."
}

println(Cat("Garfield", 38, "ginger and black") show)

//What we should care is implicit scope - we defer 3 :
//1) Local scope
//2) imported scope - show example!
//3) companion object scope

//Type enrichment - example


// You think You do not need it - so how You do range in scala?
// excercise - create method times which will apply function n times to int - 3* List(1,2) => List (1,2,1,2,1,2) - 10min

//Monads and F[_}
import cats.Monad
import cats.syntax.functor._ // for map
import cats.syntax.flatMap._ // for flatMap
import scala.language.higherKinds // uuu be careful

def sumSquare[F[_]: Monad](a: F[Int], b: F[Int]): F[Int] =
  a.flatMap(x => b.map(y => x*x + y*y))
//how You can refactor sumSquare

import cats.instances.option._ // for Monad
import cats.instances.list._ // for Monad

sumSquare(Option(3), Option(4))

sumSquare(List(1, 2, 3), List(4, 5))

// Now this will not work
//sumSquare(4 , 5)

//remark why we need it