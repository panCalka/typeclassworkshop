
class TypeClassEqual {
  def main(args: Array[String]): Unit = {

    trait Equality[L, R] {
      def equals(left: L, right: R): Boolean
    }

    object Equality{
      def apply[T, L](left: T, right: L)(implicit ev: Equality[T, L]): Boolean = ev.equals(left,right)
    }

    //this works
      implicit def sameType[T] = new Equality[T, T] {
        override def equals(left: T, right: T): Boolean = left.equals(right)
    }

    println(Equality.apply( BigDecimal(3,3), BigDecimal(3,3)))

    println(Equality.apply( BigDecimal(3,3), 3.3))

  }
}
