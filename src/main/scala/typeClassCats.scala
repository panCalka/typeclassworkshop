import scala.language.postfixOps

object typeClassCats extends App {

  trait Printable[A] {
    def format(value: A): String
  }

  object PrintableInstances {

    implicit def stringPrintable = new Printable[String] {
      def format(input: String) = input
    }

    implicit def intPrintable = new Printable[Int] {
      def format(value: Int): String = value.toString
    }
  }

  object Printable {
    def format[A](input: A)(implicit p: Printable[A]): String =
      p.format(input)

    def print[A](input: A)(implicit p: Printable[A]): Unit =
      println(format(input))
  }

  final case class Cat(name: String, age: Int, color: String)

  import PrintableInstances._

  implicit val catPrintable = new Printable[Cat] {
    def format(cat: Cat) = {
      val name = Printable.format(cat.name)
      val age = Printable.format(cat.age)
      val color = Printable.format(cat.color)
      s"$name is a $age year-old $color cat."
    }
  }

  val cat =  Cat("Garfield", 38, "ginger and black")
  Printable.print(cat)

 // ---------------------Type enrichment----------------------


  object PrintableSyntax {

    implicit class PrintableOps[A](value: A) {
      def format(implicit p: Printable[A]): String =
        p.format(value)

      def print(implicit p: Printable[A]): Unit =
        println(p.format(value))
    }
  }

  import PrintableSyntax._

  Cat("Garfield", 38, "ginger and black") print

  import cats.Show
  import cats.instances.int._ // for Show
  import cats.instances.string._ // for Show
  import cats.syntax.show._ // for show

  implicit val catShow = Show.show[Cat] { cat =>
    val name = cat.name.show
    val age = cat.age.show
    val color = cat.color.show
    s"$name is a $age year-old $color cat."
  }

  println(Cat("Garfield", 38, "ginger and black") show)
}